public class SpringCheatSheet {
    /*Spring initializer
    group - artifact - option > JAVA 13
    telecharger le zip
    ça nous génère un fichier Maven pom
    copier / coller ce qu'il y a l'intérieur de ce fichier pom à l'intérieur du projet extstant (pom)!! faire attention et bien comparer!
    verfifier également le gitignore
    -----------------------
    @Bean se poser la question si la classe est ou non un singleton
    (un fabricant type scanner ou TmbdClient, ils peuvent être instanciés une seule fois sans soucis)
    Injection de dépendances Spring : remonte dans le code et regarde tout se qui peut être un @Bean
    @Bean ne peuvent pas changer car instanciées une seule fois !

    @Configuration -> met toute la classe en @Component

    Factory : usine : transforme et restitue
    Refactor du code pour y poser Spring
    Command Line Runner = @PostConstruct mais attention !!!! solution de dépannage de main

    inversion de controle : n'avoir aucun @Bean ou singleton qui est initialisé (= new ....)


@Configuration - mark a class as a source of bean definitions.
@Bean - indicates that a method produces a bean to be managed by the Spring container.
@Component - turns the class into a Spring bean at the auto-scan time. @Service - specialization of the @Component, has no encapsulated state.
@Autowired - Spring’s dependency injection wires an appropriate bean into the marked class member.
@SpringBootApplication - uses @Configuration,
@Controller - marks the class as web controller, capable of handling the requests. @RestController - a convenience annotation of a @Controller and @ResponseBody.
@ResponseBody - makes Spring bind method’s return value to the web response body.
@RequestMapping - specify on the method in the controller, to map a HTTP request to the URL to this method.


     */
}
