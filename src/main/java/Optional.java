public class Optional {
    /*
    import com.zenika.academy.quiz.services.exceptions.NoMovieException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
class Scratch {
    public static void main(String[] args) {
        Map<String, String> myMap = new HashMap<>();
        // Créer un Optional
        // -----------------
        Optional<String> maybeAString = Optional.of("Obélix");
        Optional<String> maybeAString2 = Optional.empty();
        Optional<String> maybeAString3 = Optional.ofNullable(myMap.get("Toto"));
        // Transformer la valeur dans l'optional
        // --------------------------------------
        Optional<Integer> maybeAnInteger = maybeAString.map(String::length);
        Optional<Integer> maybeAnInteger2 = maybeAString2.map(str -> {
            return str.length();
        });
        // Accéder à la valeur
        //---------------------
        if(maybeAString.isPresent()) {
            String s1 = maybeAString.get();
        }
        maybeAString.ifPresent(str -> System.out.println(str));
        System.out.println(maybeAString3.orElse("Default value"));
        String stringOutsideTheOptional = maybeAString2.orElseThrow(() -> new NoMovieException("Pas de film !"));
        if(maybeAString2.isEmpty()) {
            throw new NoMovieException("Pas de film !");
        }
    }
}

@GetMapping("/{id}")
    ResponseEntity<Question> getOneQuestion(@PathVariable("id") long id) {
        Optional<Question> question = questionService.getOneQuestion(id);
        if(question.isPresent()) {
            return ResponseEntity.ok(question.get());
        }
        else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/{id}")
    ResponseEntity<Question> getOneQuestion(@PathVariable("id") long id) {
        Optional<Question> question = questionService.getOneQuestion(id);
        return question
                .map(q -> ResponseEntity.ok(q)) // Optional<ResponseEntity<Question>>
                .orElse(ResponseEntity.notFound().build()); // ResponseEntity<Question>
    }
     */

}
