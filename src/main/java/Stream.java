import java.util.List;
import java.util.stream.Collectors;

public class Stream {
/*
class Scratch {
    public static void main(String[] args) {
        final List<Integer> listOfIntegers = List.of(1, 2, 3, 4, 5, 6, 7, 8);
        // Construction d'un stream à partir d'une liste
        final Stream<Integer> oneToEight = listOfIntegers.stream();
        // Application d'une méthode sur tous les éléments du stream
        final Stream<Integer> twoToSixteen = oneToEight.map(x -> x * 2);
        // Filter certains éléments du stream
        final Stream<Integer> fourToSixteen = twoToSixteen.filter(x -> x % 4 == 0);
        // Garder seulement n éléments du stream
        final Stream<Integer> fourToTwelve = fourToSixteen.limit(3);
        // Re-construction d'une liste à partir du stream
        List<Integer> result = fourToTwelve.collect(Collectors.toList());
        System.out.println(result);
    }
}
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
class Scratch {
    public static void main(String[] args) {
        final List<Integer> listOfIntegers = List.of(1, 2, 3, 4, 5, 6, 7, 8);
        // Construction d'un stream à partir d'une liste
        final List<Integer> result = listOfIntegers.stream()
                .map(x -> x * 2)
                .filter(x -> x % 4 == 0)
                .limit(3)
                .collect(Collectors.toList());
        System.out.println(result);
    }
}
*/


}
